# Aptible Application Exercise

This is Juan Pablo Casabianca's Application exercise for the Growth-focused Front End Engineer role.

## Build Setup

This is a regular Vue.js project. Just pull the repo and you have the following options:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
